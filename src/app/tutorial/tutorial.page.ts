import { Component, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {
  @ViewChild("slides") slides;
  constructor(private storage: Storage, private router: Router) { }

  ngOnInit() {
  }

  async finish() {
    await this.storage.set('tutorialComplete', true);
  }

  next() {
    this.slides.slideNext();
  }

}
