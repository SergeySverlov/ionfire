import { Component, OnInit } from '@angular/core';
import { DbService } from '../services/db.service';
import { ModalController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.page.html',
  styleUrls: ['./todo.page.scss'],
})
export class TodoPage implements OnInit {

  constructor(
    public db: DbService,
    public modal: ModalController,
    public auth: AuthService
  ) {}

  ngOnInit() {
  }

}
