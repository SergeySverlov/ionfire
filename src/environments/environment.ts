// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBrm9JO8lgfaswD4tGKNn91_pdnI8MAz_A",
    authDomain: "ionfire-708f4.firebaseapp.com",
    databaseURL: "https://ionfire-708f4.firebaseio.com",
    projectId: "ionfire-708f4",
    storageBucket: "ionfire-708f4.appspot.com",
    messagingSenderId: "1060987265618",
    appId: "1:1060987265618:web:5ec522da0510c6dc02b58b",
    measurementId: "G-KL4BP1MMH2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
